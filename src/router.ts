import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/modules/Home/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/bitcoin-price',
      name: 'bitcoin-price',
      component: () => import('@/modules/BitcoinPrice/BitcoinPrice.vue'),
    }
  ],
});

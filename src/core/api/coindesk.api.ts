import axios, { AxiosInstance } from 'axios';

export const coindeskAPI: AxiosInstance = axios.create({
  baseURL: 'https://api.coindesk.com/v1',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
});

export default coindeskAPI;

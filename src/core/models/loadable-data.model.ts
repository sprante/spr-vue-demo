export interface LoadableData<T = any> {
  data: T | null,
  error: Error | null,
  isLoading: boolean,
}

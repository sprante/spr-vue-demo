import Vue from 'vue';
import vueRouter from 'vue-router';
import Vuetify from 'vuetify'

Vue.use(Vuetify, {
  iconfont: 'md',
});

import { shallowMount, RouterLinkStub } from '@vue/test-utils';

import App from './App.vue';

describe('App.vue component matches snapshot', () => {
  it('for default view', () => {
    Vue.use(vueRouter);
    const wrapper = shallowMount(App, {
      stubs: {
        RouterLink: RouterLinkStub,
      },
    });
    expect(wrapper).toMatchSnapshot();
  });
});

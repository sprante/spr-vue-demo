import coindeskAPI from '@/core/api/coindesk.api';

export const cryptoTickerAPI = {
  async getAll(): Promise<any> {
    return await coindeskAPI.get('/currency/ticker?currencies=ADA,BCH,BSV,BTC,BTG,DASH,DCR,DOGE,EOS,ETC,ETH,IOTA,LSK,LTC,NEO,QTUM,TRX,XEM,XLM,XMR,XRP,ZEC');
  },
  async getSome(ids: string[]): Promise<any> {
    return await coindeskAPI.get('/currency/ticker?currencies=' + ids.join(','));
  }
}

export default cryptoTickerAPI;
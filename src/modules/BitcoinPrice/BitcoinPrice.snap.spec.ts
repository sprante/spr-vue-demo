import Vue from 'vue';
import Vuetify from 'vuetify'

import { shallowMount, Wrapper } from '@vue/test-utils';

import BitcoinPrice from './BitcoinPrice.vue';

Vue.use(Vuetify, {
  iconfont: 'md',
});

describe('BitcoinPrice.vue matches snapshot', () => {
  let wrapper: Wrapper<BitcoinPrice>;

  afterEach(() => {
    wrapper.destroy();
    jest.clearAllMocks();
  })

  it('with bitcoin data pending', () => {
    wrapper = shallowMount(BitcoinPrice, {
      computed: {
        isLoading: () => true,
        isError: () => false,
        currencies: () => [],
      },
    });
    expect(wrapper).toMatchSnapshot();
  });

  it('with bitcoin data successfully loaded', () => {
    wrapper = shallowMount(BitcoinPrice, {
      computed: {
        isLoading: () => false,
        isError: () => false,
        currencies: () => [{
          code: 'USD',
          description: 'description of USD',
          rate_float: 123,
        }],
      },
    });
    expect(wrapper).toMatchSnapshot();
  });

  it('with bitcoin data error', () => {
    wrapper = shallowMount(BitcoinPrice, {
      computed: {
        isLoading: () => false,
        isError: () => true,
        currencies: () => [],
      },
    });
    expect(wrapper).toMatchSnapshot();
  });
});

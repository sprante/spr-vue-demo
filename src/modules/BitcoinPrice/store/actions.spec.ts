import { createLocalVue } from '@vue/test-utils'
import Vuex, { Store, MutationTree } from 'vuex';
import { AxiosResponse } from 'axios';

import coindeskAPI from '@/core/api/coindesk.api';
import { BitcoinPriceState } from '../models';
import { storeOptions, Mutations } from './index';

jest.mock('@/core/api/coindesk.api', () => ({
  get: jest.fn(),
}));

const localVue = createLocalVue();
localVue.use(Vuex);

describe('BitcoinPrice/store actions', () => {
  const coindeskGetMock = coindeskAPI.get as jest.Mock;
  let store: Store<BitcoinPriceState>;
  let mutations: MutationTree<BitcoinPriceState>;

  beforeEach(() => {
    mutations = {
      [Mutations.GetPricePending]: jest.fn(),
      [Mutations.GetPriceSuccess]: jest.fn(),
      [Mutations.GetPriceError]: jest.fn(),
    };
    store = new Vuex.Store({ ...storeOptions, mutations });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('handles fetchBitcoinPrice (success response)', async () => {
    expect.assertions(3);

    coindeskGetMock.mockImplementationOnce(() => new Promise<Partial<AxiosResponse>>(
      (resolve, reject) => resolve({ status: 200, data: { a: 'data a' } })
    ));
     
    await store.dispatch('fetchBitcoinPrice');

    expect(mutations[Mutations.GetPricePending]).toHaveBeenCalledTimes(1);
    expect(mutations[Mutations.GetPriceSuccess]).toHaveBeenCalledTimes(1);
    expect(mutations[Mutations.GetPriceError]).toHaveBeenCalledTimes(0);
  });

  it('handles fetchBitcoinPrice (error response)', async () => {
    expect.assertions(5);

    coindeskGetMock.mockImplementationOnce(() => new Promise<Partial<AxiosResponse>>(
      (resolve, reject) => reject({ data: new Error('Not found'), status: 404 })
    ));

    await store.dispatch('fetchBitcoinPrice');

    expect(mutations[Mutations.GetPricePending]).toHaveBeenCalledTimes(1);
    expect(mutations[Mutations.GetPricePending]).toHaveBeenCalledWith({
      btcPrice: {
        data: null,
        error: null,
        isLoading: false,
      },
    }, undefined);

    expect(mutations[Mutations.GetPriceSuccess]).toHaveBeenCalledTimes(0);
  
    expect(mutations[Mutations.GetPriceError]).toHaveBeenCalledTimes(1);
    expect(mutations[Mutations.GetPriceError]).toHaveBeenCalledWith({
      btcPrice: {
        data: null,
        error: null,
        isLoading: false,
      },
    }, { data: new Error('Not found'), status: 404 });
  });
});

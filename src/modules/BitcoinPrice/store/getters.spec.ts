import { storeOptions } from './index';

describe('BitcoinPrice/store getters', () => {
  describe('currencies', () => {
    it('when there is data', () => {
      const state = {
        btcPrice: {
          data: null,
          error: null,
          isLoading: false,
        },
      };
      const value = (storeOptions.getters as any).currencies(state);

      expect(value).toEqual([]);
    });

    it('when there is no data', () => {
      const state = {
        btcPrice: {
          data: {
            USD: { code: 'USD' },
            GBP: { code: 'GBP' },
          },
          error: null,
          isLoading: false,
        },
      };
      const value = (storeOptions.getters as any).currencies(state);

      expect(value).toEqual([{ code: 'USD' }, { code: 'GBP' }]);
    });
  });

  describe('isLoading', () => {
    const state = {
      btcPrice: {
        data: null,
        error: null,
        isLoading: true,
      },
    };
    const value = (storeOptions.getters as any).isLoading(state);

    expect(value).toBe(true);
  });

  describe('isError', () => {
    const state = {
      btcPrice: {
        data: null,
        error: new Error('Not Found'),
        isLoading: false,
      },
    };
    const value = (storeOptions.getters as any).isError(state);

    expect(value).toBe(true);
  });
});

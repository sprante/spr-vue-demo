import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { AxiosResponse } from 'axios';

import { bitcoinPriceAPI } from '../api';
import { BitcoinCurrency, BitcoinPriceState } from '../models';

function delay<T>(millis: number, value?: T): Promise<T> {
  return new Promise((resolve) => setTimeout(() => resolve(value), millis))
}

Vue.use(Vuex);

export enum Mutations {
  GetPricePending = 'BitcoinPrice/GET_PRICE_PENDING',
  GetPriceSuccess = 'BitcoinPrice/GET_PRICE_SUCCESS',
  GetPriceError = 'BitcoinPrice/GET_PRICE_ERROR',
}

export const storeOptions: StoreOptions<BitcoinPriceState> = {
  strict: process.env.NODE_ENV !== 'production',
  state: {
    btcPrice: {
      data: null,
      error: null,
      isLoading: false
    },
  },
  mutations: {
    [Mutations.GetPricePending](state: BitcoinPriceState) {
      state.btcPrice = {
        data: null,
        isLoading: true,
        error: null,
      }
    },
    [Mutations.GetPriceSuccess](state: BitcoinPriceState, {data}: AxiosResponse<any>) {
      state.btcPrice = {
        ...state.btcPrice,
        data: data.bpi,
        isLoading: false,
      };
    },
    [Mutations.GetPriceError](state: BitcoinPriceState, error: Error) {
      state.btcPrice = {
        ...state.btcPrice,
        isLoading: false,
        error,
      }
    }
  },
  actions: {
    async fetchBitcoinPrice({ commit }: any)  {
      try {
        commit(Mutations.GetPricePending);
        commit(Mutations.GetPriceSuccess, await bitcoinPriceAPI.get().then(value => delay<any>(1000, value)));
      } catch (err) {
        commit(Mutations.GetPriceError, err);
      }
    }
  },
  getters: {
    currencies(state: BitcoinPriceState): BitcoinCurrency[] {
      return !!state.btcPrice.data ? Object.values(state.btcPrice.data) : [];
    },
    isLoading(state: BitcoinPriceState): boolean {
      return state.btcPrice.isLoading;
    },
    isError(state: BitcoinPriceState): boolean {
      return !!state.btcPrice.error;
    }
  }
};

export default new Vuex.Store(storeOptions);

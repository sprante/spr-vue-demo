import { MutationTree } from 'vuex';

import { BitcoinPriceState } from '../models';
import { storeOptions, Mutations } from './index';

describe('BitcoinPrice/store mutations', () => {
  let state: BitcoinPriceState;
  let mutations: MutationTree<BitcoinPriceState>;

  beforeEach(() => {
    mutations = storeOptions.mutations as MutationTree<BitcoinPriceState>;
    state = {
      btcPrice: {
        data: null,
        error: null,
        isLoading: false
      },
    };
  });

  it(Mutations.GetPricePending, () => {
    const mutation = mutations[Mutations.GetPricePending];
    expect(mutation).toBeTruthy();

    mutation(state);
    expect(state.btcPrice.data).toBeNull();
    expect(state.btcPrice.error).toBeNull();
    expect(state.btcPrice.isLoading).toBe(true);
  });

  it(Mutations.GetPriceSuccess, () => {
    const mutation = mutations[Mutations.GetPriceSuccess];
    expect(mutation).toBeTruthy();

    const data = { bpi: { value: 'a' } };

    mutation(state, { data });
    expect(state.btcPrice.data).toEqual(data.bpi);
    expect(state.btcPrice.error).toBeNull();
    expect(state.btcPrice.isLoading).toBe(false);
  });

  it(Mutations.GetPriceError, () => {
    const mutation = mutations[Mutations.GetPriceError];
    expect(mutation).toBeTruthy();

    const error = new Error('Get price error');

    mutation(state, error);
    expect(state.btcPrice.data).toBeNull();
    expect(state.btcPrice.error).toEqual(error);
    expect(state.btcPrice.isLoading).toBe(false);
  });
});

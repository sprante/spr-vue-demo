import coindeskAPI from '@/core/api/coindesk.api';

export const bitcoinPriceAPI = {
  async get(): Promise<any> {
    return await coindeskAPI.get('/bpi/currentprice.json');
  }
}

export default bitcoinPriceAPI;
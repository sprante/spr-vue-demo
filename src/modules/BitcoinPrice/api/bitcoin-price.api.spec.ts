import { AxiosResponse } from 'axios';

import coindeskAPI from '@/core/api/coindesk.api';
import bitcoinPriceAPI from './bitcoin-price.api';

jest.mock('@/core/api/coindesk.api', () => ({
  get: jest.fn(),
}));

describe('bitcoin-price.api', () => {
  const coindeskGetMock = coindeskAPI.get as jest.Mock;
  describe('get()', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });
  
    it('should make a successful request through core/coindeskAPI', async () => {
      expect.assertions(4);

      coindeskGetMock.mockImplementationOnce(() => new Promise<Partial<AxiosResponse>>(
        (resolve, reject) => resolve({ status: 200, data: { a: 'data a' } })
      ))
  
      const httpResponse = await bitcoinPriceAPI.get();

      expect(coindeskGetMock).toHaveBeenCalledWith('/bpi/currentprice.json');
      expect(coindeskGetMock).toHaveBeenCalledTimes(1);
      expect(httpResponse.data).toEqual({ a: 'data a' });
      expect(httpResponse.status).toBe(200);
    });

    it('should make an unsuccessful request through core/coindeskAPI', async () => {
      let httpResponse: AxiosResponse;
    
      expect.assertions(4);

      coindeskGetMock.mockImplementationOnce(() => new Promise<Partial<AxiosResponse>>(
        (resolve, reject) => reject({ status: 404, data: new Error('Not found') })
      ))

      try {
        httpResponse = await bitcoinPriceAPI.get();
      } catch (error) {
        httpResponse = error;
      }

      expect(coindeskGetMock).toHaveBeenCalledWith('/bpi/currentprice.json');
      expect(coindeskGetMock).toHaveBeenCalledTimes(1);
      expect(httpResponse.data.message).toEqual('Not found');
      expect(httpResponse.status).toBe(404);
    });
  });
});
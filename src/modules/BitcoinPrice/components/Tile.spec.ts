import Vue from 'vue';
import vueRouter from 'vue-router';
import Vuetify from 'vuetify'

import { shallowMount, Wrapper } from '@vue/test-utils';

import Tile from './Tile.vue';

Vue.use(Vuetify, {
  iconfont: 'md',
});
Vue.use(vueRouter);

const currency: jest.Mock = jest.fn();

describe('BitcoinPrice/Tile.vue', () => {
  let wrapper: Wrapper<Tile>;

  afterEach(() => {
    wrapper.destroy();
    jest.clearAllMocks();
  });

  it('renders without error', () => {
    wrapper = shallowMount(Tile, {
      propsData: { code: 'USD', rate_float: 5526.42 },
    });
    expect(wrapper).toBeTruthy();
  });

  it('calls the currency filter when rendering', () => {
    wrapper = shallowMount(Tile, {
      propsData: { code: 'USD', rate_float: 5526.42 },
      filters: { currency },
    });

    expect(currency).toHaveBeenCalledWith(5526.42, 'USD');

    wrapper.setProps({ code: 'USDx' });

    expect(currency).toHaveBeenCalledWith(5526.42, 'USDx');
  });
});

import Vue from 'vue';
import vueRouter from 'vue-router';
import Vuetify from 'vuetify'

import { shallowMount, Wrapper } from '@vue/test-utils';

import Tile from './Tile.vue';

Vue.use(Vuetify, {
  iconfont: 'md',
});
Vue.use(vueRouter);

describe('BitcoinPrice/Tile.vue matches snapshot', () => {
  let wrapper: Wrapper<Tile>;

  afterEach(() => {
    wrapper.destroy();
    jest.clearAllMocks();
  });

  it('with a valid currency code', () => {
    wrapper = shallowMount(Tile, {
      propsData: { code: 'USD', rate_float: 5526.42 },
    });
    expect(wrapper).toMatchSnapshot();
  });

  it('with an invalid currency code', () => {
    wrapper = shallowMount(Tile, {
      propsData: { code: 'USDx', rate_float: 5526.42 },
    });
    expect(wrapper).toMatchSnapshot();
  });
});

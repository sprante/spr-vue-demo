import { LoadableData } from '@/core/models/loadable-data.model';
import { BitcoinCurrency } from './bitcoin-currency.model';

export interface BitcoinPriceState {
  btcPrice: LoadableData<{[key: string]: BitcoinCurrency}>
}

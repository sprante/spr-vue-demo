export interface BitcoinCurrency {
  code: string;
  symbol: string;
  rate: string;
  description: string;
  rate_float: number;
}

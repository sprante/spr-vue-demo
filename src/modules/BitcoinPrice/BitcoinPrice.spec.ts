import Vue from 'vue';
import Vuetify from 'vuetify'

import { shallowMount } from '@vue/test-utils';

import BitcoinPrice from './BitcoinPrice.vue';

Vue.use(Vuetify, {
  iconfont: 'md',
});

describe('BitcoinPrice.vue', () => {
  it('renders without error', () => {
    const wrapper = shallowMount(BitcoinPrice);
    expect(wrapper).toBeTruthy();
  });
});

import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { HomeState } from '../models';

Vue.use(Vuex);

export const store: StoreOptions<HomeState> = {
  state: {
    appTopics: [
      {
        name: 'Vue',
        avatar: require('@/assets/vue_logo.png'),
      },
      {
        name: 'VueX',
        avatar: 'http://www.gravatar.com/avatar',
      },
      {
        name: 'Jest',
        avatar: 'http://www.gravatar.com/avatar',
      },
      {
        name: 'Axios',
        avatar: 'http://www.gravatar.com/avatar',
      },
      {
        name: 'ESLint',
        avatar: 'http://www.gravatar.com/avatar',
      },
      {
        name: 'Vuetify',
        avatar: require('@/assets/vuetify_logo.svg'),
      },
      {
        name: 'TypeScript',
        avatar: 'http://www.gravatar.com/avatar',
      },
      {
        name: 'SCSS',
        avatar: 'http://www.gravatar.com/avatar',
      },
      {
        name: 'Class Components',
        avatar: 'http://www.gravatar.com/avatar',
      },
      {
        name: 'Responsive Design',
        avatar: 'http://www.gravatar.com/avatar',
      }
    ],
  },
  getters: {
    topics(state: HomeState): { name: string }[] {
      return state.appTopics;
    }
  },
  mutations: {

  },
  actions: {

  },
};

export default new Vuex.Store(store);

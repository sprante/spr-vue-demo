import Vue from 'vue';
import vueRouter from 'vue-router';
import Vuetify from 'vuetify'

Vue.use(Vuetify, {
  iconfont: 'md',
});

import { shallowMount } from '@vue/test-utils';

import Home from './Home.vue';

describe('Home.vue component matches snapshot', () => {
  it('with items in list', () => {
    Vue.use(vueRouter);
    const wrapper = shallowMount(Home, {
      computed: {
        topics: () => [{ name: 'a' }, { name: 'b' }, { name: 'c' }]
      }
    });
    expect(wrapper).toMatchSnapshot();
  });

  it('without items in list', () => {
    Vue.use(vueRouter);
    const wrapper = shallowMount(Home, {
      computed: {
        topics: () => []
      }
    });
    expect(wrapper).toMatchSnapshot();
  });
});